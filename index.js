// Load protobuf definitions
const protobuf = require('protobufjs')
const root = protobuf.loadSync('proto/updateCustomer.proto')
const updateCustomerMessage = root.lookupType('customer.UpdateCustomer')

// Construct sample payload
const payload = {
  customer: { name: 'Foo', address: { city: 'SomePlace' } },
  someOtherField: 'Bar',
}

console.log('Initial Payload:', payload)

// Check for errors in payload
const err = updateCustomerMessage.verify(payload)
if (err) {
  throw Error(err)
}

console.log('JSON String length:', JSON.stringify(payload).length)

// Create Protobuf message
const message = updateCustomerMessage.create(payload)
console.log('Protobuf Message:', message)

// Encode to buffer
const buffer = updateCustomerMessage.encode(message).finish()
console.log('Buffer:', buffer)
console.log('Buffer Length:', buffer.length)

// Decode buffer back to JSON
const decoded = updateCustomerMessage.decode(buffer).toJSON()
console.log('Decoded JSON:', decoded)
