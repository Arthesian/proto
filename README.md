# Proto Test

This repo is to test nested Protobuf messages

Install all dependencies:

> yarn

Run script:

> node index

Output should look like:

```
Initial Payload: {
  customer: { name: 'Foo', address: { city: 'SomePlace' } },
  someOtherField: 'Bar'
}
JSON String length: 81
Protobuf Message: UpdateCustomer {
  someList: [],
  customer: { name: 'Foo', address: { city: 'SomePlace' } },
  someOtherField: 'Bar'
}
Buffer: <Buffer 0a 12 0a 03 46 6f 6f 12 0b 0a 09 53 6f 6d 65 50 6c 61 63 65 12 03 42 61 72>
Buffer Length: 25
Decoded JSON: {
  customer: { name: 'Foo', address: { city: 'SomePlace' } },
  someOtherField: 'Bar'
}
```
